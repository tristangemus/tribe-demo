<?php
/*
Plugin Name: Woocommerce Klaviyo Integration
Description: Woocommerce Klaviyo Integration
*/

define( 'WCK_ROOT', __DIR__ );
define( 'WCK_LOCALE', 'woocommerce-klaviyo' );
define( 'WCK_VERSION', '1.0.0' );

include WCK_ROOT . '/includes/klaviyo.php';
include WCK_ROOT . '/includes/class-wck-hooks.php';

$GLOBALS['WCK_Hooks'] = new WCK_Hooks();
