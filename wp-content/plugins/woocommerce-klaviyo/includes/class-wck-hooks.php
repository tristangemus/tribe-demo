<?php

class WCK_Hooks {

	const TOKEN = 'MKXXDQ';

	public function __construct() {
		add_action( 'woocommerce_checkout_subscription_created', [ $this, 'subscription_created' ], 10, 1 );
		add_action( 'woocommerce_subscription_status_pending-cancel', [ $this, 'subscription_cancel' ], 10, 1 );
	}

	public function subscription_created( $subscription ) {
		$items = $subscription->get_items();

		$client = new Klaviyo( self::TOKEN );

		foreach ( $items as $item ) {
			$is_trial = $this->is_trial( $subscription );

			$response = $client->track(
				'Subscribed to Plan',
				[ '$email' => $subscription->get_billing_email() ],
				[
					'Plan' => $item->get_name(),
					'PlanID' => $item->get_product_id(),
					'PlanType' => $is_trial ? 'Trial' : 'Paid',
					'$value' => $is_trial ? 0 : $subscription->get_total()
				],
				time()
			);
		}
	}

	public function subscription_cancel( $subscription ) {
		$items = $subscription->get_items();

		$client = new Klaviyo( self::TOKEN );

		foreach ( $items as $item ) {
			$is_trial = $this->is_trial( $subscription );
			
			$response = $client->track(
				'Cancelled Plan',
				[ '$email' => $subscription->get_billing_email() ],
				[
					'Plan' => $item->get_name(),
					'PlanID' => $item->get_product_id(),
					'PlanType' => $is_trial ? 'Trial' : 'Paid',
					'$value' => $is_trial ? 0 : $subscription->get_total()
				],
				time()
			);
		}
	}

	private function is_trial( $subscription ) {
		return false != $subscription->get_date( 'trial_end' );
	}

}