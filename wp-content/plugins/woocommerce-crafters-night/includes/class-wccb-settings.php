<?php

class WCCB_Settings {

	public function __construct() {
		add_filter( 'woocommerce_get_sections_products', [ $this, 'add_settings_section' ], 10, 1 );
		add_filter( 'woocommerce_get_settings_products', [ $this, 'display_settings' ], 10, 2 );
	}

	public function add_settings_section( $sections ) {
		$sections['crafters_subscriptions'] = __( 'Crafter\'s Box', WCCN_LOCALE );

		return $sections;
	}

	public function display_settings( $settings, $current_section ) {
		if ( 'crafters_subscriptions' === $current_section ) {
			$products = get_posts( [
				'post_type' => 'product',
				'post_status' => 'publish',
				'posts_per_page' => -1
			] );

			$options = [];

			foreach ( $products as $product ) {
				$options[ $product->ID ] = $product->post_title;
			}

			return [
				[
					'name' => __( 'Crafter\'s Box Subscription Settings', WCCN_LOCALE ),
					'type' => 'title'
				],
				[
					'name' => __( 'Product', WCCN_LOCALE ),
					'id' => 'crafters_subscription_product_id',
					'type' => 'select',
					'options' => $options
				],
				[
					'type' => 'sectionend',
					'id' => 'crafters_subscriptions'
				]
			];
		}

		return $settings;
	}

}
