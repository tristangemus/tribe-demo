<?php

class WCCB_Switcher {

	public function __construct() {
		add_filter( 'woocommerce_subscriptions_switch_error_message', [ $this, 'attempt_switch' ], 10, 6 );
	}

	public function attempt_switch( $error_message, $product_id, $quantity, $variation_id, $subscription, $item ) {
		$variation = new WC_Product_Variation( $variation_id );
		$new_num_guests = intval( $variation->get_attribute( 'guests' ) );
		$current_subaccounts = count( $GLOBALS['WCCB_Dashboard']->subaccounts );

		if ( $current_subaccounts > $new_num_guests ) {
			return sprintf(
				__( 'You must remove %s subaccounts first!', WCCN_LOCALE ),
				$current_subaccounts - $new_num_guests
			);
		}

		return $error_message;
	}

}