<?php

class WCCB_Dashboard {

	public $user = false;

	public $product_id;

	public $active_subscription = false;

	public $num_guests;

	public function __construct() {
		add_action( 'init', [ $this, 'save' ], 10, 0 );
		add_action( 'init', [ $this, 'init' ], 11, 0 );
		add_action( 'init', [ $this, 'endpoint' ], 12, 0 );
		add_filter( 'woocommerce_account_menu_items', [ $this, 'add_users_section' ], 10, 1 );
		add_action( 'woocommerce_account_subaccounts_endpoint', [ $this, 'add_users_content' ], 10, 0 );
	}

	public function add_users_section( $items ) {
		if ( $this->active_subscription ) {
			$items['subaccounts'] = __( 'Sub-accounts', WCCN_LOCALE );
		}

		return $items;
	}

	public function endpoint() {
		add_rewrite_endpoint( 'subaccounts', EP_PAGES );
	}

	public function init() {
		$this->user = wp_get_current_user();
		$this->subscriptions = wcs_get_users_subscriptions( $this->user->ID );
		$this->product_id = intval( get_option( 'crafters_subscription_product_id', 0 ) );

		foreach ( $this->subscriptions as $subscription ) {
			if ( 'active' === $subscription->get_status() ) {
				$items = $subscription->get_items();

				foreach ( $items as $item ) {
					if ( $this->product_id === intval( $item->get_product_id() ) ) {
						$this->active_subscription = true;
						
						$variation_id = $item->get_variation_id();
						$variation = new WC_Product_Variation( $variation_id );
						$this->subscription_id = $subscription->get_id();
						$this->num_guests = intval( $variation->get_attribute( 'guests' ) );
						$this->subaccounts = get_users( [
							'meta_key' => 'crafters_night_subaccount',
							'meta_value' => $subscription->get_id(),
							'meta_compare' => '='
						] );

						break;
					}
				}
			}
		}
	}

	public function save() {
		if ( isset( $_POST['action'] ) && 'add_sub_account' === $_POST['action'] ) {
			$email = sanitize_text_field( $_POST['account_email'] );

			if ( false == ( $user = get_user_by( 'email', $email ) ) ) {
				$password_string = wp_generate_password( 8 );

				$user_id = wp_insert_user( [
					'user_login' => $email,
					'user_pass' => $password_string,
					'user_email' => $email,
					'first_name' => sanitize_text_field( $_POST['account_first_name'] ),
					'last_name' => sanitize_text_field( $_POST['account_last_name'] )
				] );
			} else {
				$user_id = $user->ID;
			}

			if ( ! is_wp_error( $user_id ) ) {
				add_user_meta( $user_id, 'crafters_night_subaccount', sanitize_text_field( $_POST['subscription_id'] ) );
				// send user an email here
			}
		}

		if ( isset( $_POST['action'] ) && 'remove_sub_account' === $_POST['action'] ) {
			$user_id = sanitize_text_field( $_POST['user_id'] );
			$subscription_id = sanitize_text_field( $_POST['subscription_id'] );

			delete_user_meta( $user_id, 'crafters_night_subaccount', $subscription_id );
		}
	}

	public function add_users_content() {
		if ( ! $this->active_subscription ) {
			return;
		}

		include WCCN_ROOT . '/views/my-account-subaccounts.php';
	}

}