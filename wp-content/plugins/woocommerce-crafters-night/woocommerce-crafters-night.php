<?php
/*
Plugin Name: Woocommerce Crafter's Night
Description: Woocommerce Crafter's Night Functionality
*/

define( 'WCCN_ROOT', __DIR__ );
define( 'WCCN_LOCALE', 'woocommerce-crafters-night' );
define( 'WCCN_VERSION', '1.0.0' );

include WCCN_ROOT . '/includes/class-wccb-dashboard.php';
include WCCN_ROOT . '/includes/class-wccb-switcher.php';
include WCCN_ROOT . '/includes/class-wccb-settings.php';

$GLOBALS['WCCB_Dashboard'] = new WCCB_Dashboard();
$GLOBALS['WCCB_Switcher'] = new WCCB_Switcher();
$GLOBALS['WCCB_Settings'] = new WCCB_Settings();
