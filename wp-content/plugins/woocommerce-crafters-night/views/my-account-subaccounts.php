<h3>Sub-accounts</h3>
<?php
	printf(
		'Available: %s/%s',
		( $this->num_guests - count( $this->subaccounts ) ),
		$this->num_guests
	);

	if ( ! empty( $this->subaccounts ) ) : ?>
		<div class="woo-subaccounts">
			<?php foreach ( $this->subaccounts as $subaccount ) : ?>
				<form action="" method="post">
					<span><?php echo $subaccount->first_name . ' ' . $subaccount->last_name; ?></span>
					<input type="hidden" name="user_id" value="<?php echo $subaccount->ID; ?>">
					<input type="hidden" name="action" value="remove_sub_account" />
					<input type="hidden" name="subscription_id" value="<?php echo $this->subscription_id; ?>">
					<input type="submit" value="Remove" />
				</form>
			<?php endforeach; ?>
		</div>
	<?php endif;

	if ( count( $this->subaccounts ) < $this->num_guests ) :
?>
<form action="" method="post">
	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name">First Name <span class="required">*</span></label>
		<input class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" type="text">
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name">Last Name <span class="required">*</span></label>
		<input class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" type="text">
	</p>
	<div class="clear"></div>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email">Email address <span class="required">*</span></label>
		<input class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" type="email">
	</p>
	<input type="hidden" name="action" value="add_sub_account" />
	<input type="hidden" name="subscription_id" value="<?php echo $this->subscription_id; ?>">
	<div class="clear"></div>
	<button type="submit" class="woocommerce-Button button" name="save_sub_account" value="Add User">Add User</button>
</form>
<?php endif; ?>