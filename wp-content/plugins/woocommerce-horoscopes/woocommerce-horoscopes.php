<?php
/*
Plugin Name: Woocommerce Horoscopes
Description: Horoscope functionality for Woocommerce
*/

define( 'WCH_ROOT', __DIR__ );
define( 'WCH_LOCALE', 'woocommerce-horoscopes' );
define( 'WCH_VERSION', '1.0.0' );

include WCH_ROOT . '/includes/post-type-video.php';
include WCH_ROOT . '/includes/product.php';

$GLOBALS['WCH_Post_Type_Video'] = new WCH_Post_Type_Video();
$GLOBALS['WCH_Product'] = new WCH_Product();
