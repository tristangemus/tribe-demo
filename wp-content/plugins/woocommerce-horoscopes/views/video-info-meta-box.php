<div>
	<label for="youtube_video_id">Youtube Video ID</label>
	<input type="text" name="youtube_video_id" id="youtube_video_id" value="<?php echo $youtube_video_id; ?>" />
</div>

<div>
	<label for="assigned_product">Assigned Product</label>
	<select name="assigned_product" id="assigned_product">
		<option value="">Select Product</option>
		<?php foreach ( $products as $product ) {
			echo sprintf(
				'<option value="%s"%s>%s</option>',
				$product->ID,
				selected( $product->ID, $assigned_product, false ),
				$product->post_title
			);
		} ?>
	</select>
</div>

<div>
	<label for="zodiac">Zodiac</label>
	<select name="zodiac" id="zodiac">
		<option value="">Select Product</option>
		<?php foreach ( WCH_Post_Type_Video::$zodiacs as $zodiac ) {
			echo sprintf(
				'<option value="%s"%s>%s</option>',
				$zodiac['key'],
				selected( $zodiac['key'], $selected_zodiac, false ),
				$zodiac['label']
			);
		} ?>
	</select>
</div>

<script>
	jQuery(document).ready(function() {
		jQuery('.datepicker').datepicker();
	});
</script>