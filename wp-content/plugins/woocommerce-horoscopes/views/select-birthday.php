<h3>Enter your birthday</h3>
<select name="birthday_month" id="birthday_month">
	<option value="">Month</option>
	<?php for ( $i = 1; $i < 13; $i++ )  {
		printf(
			'<option value="%s" %s>%s</option>',
			strval( $i ),
			selected( $i, $birthday_month, false ),
			date( 'F', mktime( 0, 0, 0, $i, 10 ) )
		);
	} ?>
</select>
<select name="birthday_day" id="birthday_day">
	<option value="">Day</option>
	<?php for ( $i = 1; $i < 32; $i++ )  {
		printf(
			'<option value="%s" %s>%s</option>',
			strval( $i ),
			selected( $i, $birthday_day, false ),
			strval( $i )
		);
	} ?>
</select>