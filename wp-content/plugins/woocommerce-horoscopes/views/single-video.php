<div class="single-video-outer">
	<p><?php _e( 'Thanks for purchasing! Here\'s your horoscope:', WCH_LOCALE ); ?></p>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	<?php echo $post_content; ?>
</div>