<?php

class WCH_Product {
	public function __construct() {
		add_filter( 'wc_get_template_part', [ $this, 'archive_template'], 10, 3 );
		add_filter( 'woocommerce_add_to_cart_validation', [ $this, 'add_to_cart_validation'], 10, 3 );
		add_filter( 'woocommerce_add_to_cart_redirect', [ $this, 'redirect_to_cart'], 10, 1 );
		add_filter( 'the_content', [ $this, 'video_content_filter' ], 11, 1 );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
		add_action( 'wp_ajax_set_date', [ $this, 'ajax_set_date' ] );
		add_action( 'wp_ajax_nopriv_set_date', [ $this, 'ajax_set_date' ] );
		add_action( 'woocommerce_single_product_summary', [ $this, 'add_birthday_field' ], 21 );
		add_action( 'woocommerce_thankyou', [ $this, 'redirect_after_checkout' ], 10, 1 );
		add_action( 'init', [ $this, 'start_session' ] ); 
	}

	public function archive_template( $template, $slug, $name ) {
		if ( $name ) {
			$path = WCH_ROOT . '/views/' . $slug . '-' . $name . '.php';
		}

		return file_exists( $path ) ? $path : $template;
	}

	public function enqueue_scripts() {
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_style( 'jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
		wp_enqueue_style( 'wch_product', plugins_url( '../assets/wch-style.css', __FILE__ ), [], WCH_VERSION );
		wp_enqueue_script( 'wch_product', plugins_url( '../assets/wch-product.js', __FILE__ ), ['jquery', 'jquery-ui-datepicker'], WCH_VERSION, true );
		wp_localize_script( 'wch_product', 'WCHData', [
			'ajax_url' => admin_url( 'admin-ajax.php' )
		] );
	}

	public function add_to_cart_validation( $true, $product_id, $quantity ) {
		if ( PHP_SESSION_NONE === session_status() ) {
			session_start();
		}

		if ( ! isset( $_SESSION['date'] ) ) {
			wc_add_notice( 'You must enter your birthday!' );

			return false;
		}

		$day_of_year = intval( $_SESSION['date']['day_of_year'] );
		$zodiac_key = '';

		foreach ( WCH_Post_Type_Video::$zodiacs as $zodiac ) {
			if ( $day_of_year >= $zodiac['day'] ) {
				$zodiac_key = $zodiac['key'];
			}
		}

		$videos = get_posts( [
			'post_type' => 'video',
			'post_status' => 'publish',
			'posts_per_page' => 1,
			'meta_query' => [
				[
					'key' => 'zodiac',
					'value' => $zodiac_key,
					'compare' => '='
				]
			]
		] );

		if ( empty( $videos ) ) {
			wc_add_notice( 'No matches for that birthday... something went wrong.' );
			return false;
		}

		$assigned_product = get_post_meta( $videos[0]->ID, 'assigned_product', true );

		if ( $assigned_product != $product_id ) {
			wc_add_notice( sprintf(
				'This is not your horoscope, please click <a href="%s">here</a> for yours!',
				get_permalink( $assigned_product )
			) );

			return false;
		}

		return $true;
	}

	public function video_content_filter( $content ) {
		global $post;

		if ( isset( $post ) && 'video' === $post->post_type ) {
			ob_start();

			$video_id = get_post_meta( $post->ID, 'youtube_video_id', true );
			$post_content = $content;

			include WCH_ROOT . '/views/single-video.php';

			$content = ob_get_clean();
		}

		return $content;
	}

	public function start_session() {
		if ( PHP_SESSION_NONE == session_status() ) {
			session_start();
		}
	}

	public function add_birthday_field() {
		$birthday_month = false;
		$birthday_day = false;

		if ( isset( $_SESSION['date'] ) ) {
			$birthday_month = isset( $_SESSION['date']['month'] ) ? $_SESSION['date']['month'] : false;
			$birthday_day = isset( $_SESSION['date']['day'] ) ? $_SESSION['date']['day'] : false;
		}

		include WCH_ROOT . '/views/select-birthday.php';
	}

	public function redirect_to_cart( $url ) {
		return wc_get_cart_url();
	}

	public static function all() {
		return get_posts( [
			'post_type' => 'product',
			'posts_per_page' => -1
		] );
	}

	public function redirect_after_checkout( $order_id ) {
		$order = new WC_Order( $order_id );

		if ( 'failed' !== $order->get_status() ) {
			$items = $order->get_items();
			
			if ( ! empty( $items ) ) {
				$item = array_shift( $items );
				$id = $item->get_product_id();
				$video = WCH_Post_Type_Video::get_by_product_id( $id );
				
				if ( $video ) {
					wp_redirect( get_permalink( $video ) );
					exit;
				}
			}
		}
	}

	public function ajax_set_date() {
		if ( ! isset( $_GET['birthday_month'] ) ) {
			wp_send_json_error( [
				'message' => __( 'Must include a date' )
			], 422 );
		}

		if ( ! isset( $_GET['birthday_day'] ) ) {
			wp_send_json_error( [
				'message' => __( 'Must include a date' )
			], 422 );
		}

		$birthday_month = sanitize_text_field( $_GET['birthday_month'] );
		$birthday_day = sanitize_text_field( $_GET['birthday_day'] );

		$this->start_session();

		$_SESSION['date'] = [
			'month' => $birthday_month,
			'day' => $birthday_day,
			'day_of_year' => date( 'z', mktime( 0, 0, 0, $birthday_month, $birthday_day, 2000 ) )
		];

		wp_send_json_success( [
			'success' => true,
			'date' => $_SESSION['date']
		], 200 );
	}
}