<?php

class WCH_Post_Type_Video {

	const SLUG = 'video';
	const NAME = 'Video';
	const NAME_PLURAL = 'Videos';
	const NAME_LOWER = 'video';
	const NAME_LOWER_PLURAL = 'videos';
	const MENU_NAME = 'Videos';

	public static $zodiacs = [
		[
			'key' => 'capricorn',
			'label' => 'Capricorn',
			'day' => 0
		],
		[
			'key' => 'aquarius',
			'label' => 'Aquarius',
			'day' => 19
		],
		[
			'key' => 'pisces',
			'label' => 'Pisces',
			'day' => 49
		],
		[
			'key' => 'aries',
			'label' => 'Aries',
			'day' => 80
		],
		[
			'key' => 'taurus',
			'label' => 'Taurus',
			'day' => 110
		],
		[
			'key' => 'gemini',
			'label' => 'Gemini',
			'day' => 141
		],
		[
			'key' => 'cancer',
			'label' => 'Cancer',
			'day' => 172
		],
		[
			'key' => 'leo',
			'label' => 'Leo',
			'day' => 204
		],
		[
			'key' => 'virgo',
			'label' => 'Virgo',
			'day' => 235
		],
		[
			'key' => 'libra',
			'label' => 'Libra',
			'day' => 266
		],
		[
			'key' => 'scorpio',
			'label' => 'Scorpio',
			'day' => 296
		],
		[
			'key' => 'sagittarius',
			'label' => 'Sagittarius',
			'day' => 326
		],
		[
			'key' => 'capricorn',
			'label' => 'Capricorn',
			'day' => 356
		],
	];

	/**
	 * Actions and filters
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'register' ] );
		add_action( 'add_meta_boxes_video', [ $this, 'add_info_meta_box' ] );
		add_action( 'save_post_video', [ $this, 'video_info_meta_box_save' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_scripts' ] );
	}

	/**
	 * Register post type
	 */
	public function register() {
		$labels = [
			'name'                  => _x( self::NAME_PLURAL, 'Post Type General Name', WCH_LOCALE ),
			'singular_name'         => _x( self::NAME, 'Post Type Singular Name', WCH_LOCALE ),
			'menu_name'             => __( self::MENU_NAME, WCH_LOCALE ),
			'name_admin_bar'        => __( self::NAME_PLURAL, WCH_LOCALE ),
			'archives'              => __( self::NAME . ' Archies', WCH_LOCALE ),
			'attributes'            => __( self::NAME . ' Attributes', WCH_LOCALE ),
			'parent_item_colon'     => __( 'Parent ' . self::NAME . ':', WCH_LOCALE ),
			'all_items'             => __( 'All ' . self::NAME_PLURAL, WCH_LOCALE ),
			'add_new_item'          => __( 'Add New ' . self::NAME, WCH_LOCALE ),
			'add_new'               => __( 'Add New', WCH_LOCALE ),
			'new_item'              => __( 'New ' . self::NAME, WCH_LOCALE ),
			'edit_item'             => __( 'Edit ' . self::NAME, WCH_LOCALE ),
			'update_item'           => __( 'Update ' . self::NAME, WCH_LOCALE ),
			'view_item'             => __( 'View ' . self::NAME, WCH_LOCALE ),
			'view_items'            => __( 'View ' . self::NAME_PLURAL, WCH_LOCALE ),
			'search_items'          => __( 'Search ' . self::NAME, WCH_LOCALE ),
			'not_found'             => __( 'Not found', WCH_LOCALE ),
			'not_found_in_trash'    => __( 'Not found in Trash', WCH_LOCALE ),
			'featured_image'        => __( 'Featured Image', WCH_LOCALE ),
			'set_featured_image'    => __( 'Set featured image', WCH_LOCALE ),
			'remove_featured_image' => __( 'Remove featured image', WCH_LOCALE ),
			'use_featured_image'    => __( 'Use as featured image', WCH_LOCALE ),
			'insert_into_item'      => __( 'Insert into ' . self::NAME_LOWER, WCH_LOCALE ),
			'uploaded_to_this_item' => __( 'Uploaded to this ' . self::NAME_LOWER, WCH_LOCALE ),
			'items_list'            => __( self::NAME_PLURAL . ' list', WCH_LOCALE ),
			'items_list_navigation' => __( self::NAME_PLURAL . ' list navigation', WCH_LOCALE ),
			'filter_items_list'     => __( 'Filter ' . self::NAME_LOWER_PLURAL . ' list', WCH_LOCALE )
		];

		$args = [
			'label'                 => __( self::NAME, WCH_LOCALE ),
			'description'           => __( 'Horoscope videos', WCH_LOCALE ),
			'labels'                => $labels,
			'supports'              => [ 'title', 'revisions', 'editor' ],
			'taxonomies'            => [ 'store' ],
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'menu_icon' 			=> 'dashicons-video-alt3'
		];

		register_post_type( self::SLUG, $args );
	}

	/**
	 * Add meta box to post type
	 */
	public function add_info_meta_box() {
		add_meta_box(
			'wch_video_info_meta_box',
			__( 'Video Information', WCH_LOCALE ),
			[ $this, 'video_info_meta_box_callback' ],
			'video',
			'normal',
			'low'
		);
	}

	/**
	 * Render meta box
	 * @param  WP_Post $post WP Post Object
	 */
	public function video_info_meta_box_callback( $post ) {
		wp_nonce_field( basename( __FILE__ ), 'video_info_meta_box_nonce' );

		$youtube_video_id = get_post_meta( $post->ID, 'youtube_video_id', true );
		$assigned_product = get_post_meta( $post->ID, 'assigned_product', true );
		$selected_zodiac = get_post_meta( $post->ID, 'zodiac', true );

		$products = WCH_Product::all();

		include WCH_ROOT . '/views/video-info-meta-box.php';
	}

	/**
	 * Save the entry meta box
	 * @param  int $post_id Post ID
	 */
	public function video_info_meta_box_save( $post_id ) {
		if ( ! isset( $_POST['video_info_meta_box_nonce'] ) || 
			 ! wp_verify_nonce( $_POST['video_info_meta_box_nonce'], basename( __FILE__ ) ) ) {
			return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		if ( isset( $_POST['youtube_video_id'] ) ) {
			update_post_meta( $post_id, 'youtube_video_id', sanitize_text_field( $_POST['youtube_video_id'] ) );
		} else {
			delete_post_meta( $post_id, 'youtube_video_id' );
		}

		if ( isset( $_POST['assigned_product'] ) ) {
			update_post_meta( $post_id, 'assigned_product', sanitize_text_field( $_POST['assigned_product'] ) );
		} else {
			delete_post_meta( $post_id, 'assigned_product' );
		}

		if ( isset( $_POST['zodiac'] ) ) {
			update_post_meta( $post_id, 'zodiac', sanitize_text_field( $_POST['zodiac'] ) );
		} else {
			delete_post_meta( $post_id, 'zodiac' );
		}
	}

	private function date_beginning( $date_str ) {
		return strtotime( 'midnight', strtotime( $date_str ) );
	}

	private function date_end( $date_str ) {
		return strtotime( 'tomorrow', strtotime( $date_str ) ) - 1;
	}

	public static function format_date( $timestamp = false ) {
		return $timestamp ? date( 'F j, Y', $timestamp ) : '';
	}

	public function admin_scripts() {
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_style( 'jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
	}

	public static function get_by_product_id( $product_id ) {
		$videos = get_posts( [
			'post_type' => 'video',
			'post_status' => 'publish',
			'posts_per_page' => 1,
			'meta_query' => [
				[
					'key' => 'assigned_product',
					'value' => $product_id,
					'compare' => '='
				]
			]
		] );

		return ! empty( $videos ) ? array_shift( $videos ) : false;
	}

}
