jQuery(document).ready(function() {
	jQuery('.datepicker').datepicker();

	jQuery('#birthday_month, #birthday_day').on('change', function(e) {
		var birthday_month = jQuery('#birthday_month').val();
		var birthday_day = jQuery('#birthday_day').val();

		if ( '' != birthday_month && '' != birthday_day ) {
			jQuery('.single_add_to_cart_button').prop('disabled', true);

			jQuery.ajax({
				url: WCHData.ajax_url,
				type: 'GET',
				data: {
					birthday_month: birthday_month,
					birthday_day: birthday_day,
					action: 'set_date'
				},
				dataType: 'json',
				success: function(data) {
					jQuery('.single_add_to_cart_button').prop('disabled', false);
					console.log(data);
				},
				error: function(request, error) {
					jQuery('.single_add_to_cart_button').prop('disabled', false);
					console.log(request);
					console.log(error);
				}
			});
		}
	});
});